<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="assets/dist/css/main.css">
    <script src="assets/dist/js/modernizr.min.js"></script>
</head>

<body>

    <!--[if lt IE 7]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>'Allo, 'Allo!</h1>
                <p>You now have</p>
                <ul>
                    <li>HTML5 Boilerplate</li>
                    <li>Bootstrap
                        <span class="glyphicon glyphicon-heart-empty" aria-hidden="true"></span>
                    </li>
                    <li>Font Awesome <i class="fa fa-flag"></i></li>
                </ul>
                <p>installed!</p>
            </div>
        </div>
    </div>

    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    <script>
    (function(b, o, i, l, e, r) {
        b.GoogleAnalyticsObject = l;
        b[l] || (b[l] =
            function() {
                (b[l].q = b[l].q || []).push(arguments)
            });
        b[l].l = +new Date;
        e = o.createElement(i);
        r = o.getElementsByTagName(i)[0];
        e.src = '//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e, r)
    }(window, document, 'script', 'ga'));
    ga('create', 'UA-XXXXX-X');
    ga('send', 'pageview');
    </script>

    <!-- jQuery -->
    <script src="assets/dist/js/jquery.min.js"></script>

    <!-- Bootstrap -->
    <script src="assets/dist/js/bootstrap.min.js"></script>

    <!-- project js -->
    <script src="assets/dist/js/main.js"></script>

    <!-- livereload -->
    <script src="//127.0.0.1:35729/livereload.js"></script>

</body>

</html>