'use strict';

module.exports = function (grunt) {

    // load all grunt tasks
    require('load-grunt-tasks')(grunt);

    // show elapsed time at the end
    require('time-grunt')(grunt);

    // configurable paths
    var appConfig = {
      rootPath: 'wwwroot',
      assetsPath: 'wwwroot/assets',
      bowerComponentsPath: 'wwwroot/assets/bower_components',
      srcPath: 'wwwroot/assets/src',
      distPath: 'wwwroot/assets/dist'
    };


    // definice uloh
    grunt.initConfig({

      appConfig: appConfig,

      clean: {
        css: {src: ['<%= appConfig.distPath %>/css/*']},
        js: {src: ['<%= appConfig.distPath %>/js/*']},
        fonts: {src: ['<%= appConfig.distPath %>/fonts/*']},
        img: {src: ['<%= appConfig.distPath %>/img/*']},
      },

      less: {
        dist: {
          files: {
            '<%= appConfig.distPath %>/css/main.css': ['<%= appConfig.srcPath %>/less/main.less']
          },
          options: {
            sourceMap: true,
          }
        }
      },

      concat: {
        options: {
          separator: ";\n\n",
          sourceMap: true
        },
        js: {
          src: '<%= appConfig.srcPath %>/js/**/*.js',
          dest: '<%= appConfig.distPath %>/js/main.js'
        }
      },

      jsbeautifier: {
          files: [
              "<%= appConfig.rootPath %>/**/*.php",
              "<%= appConfig.rootPath %>/**/*.html",
              '<%= appConfig.srcPath %>/js/**/*.js',
              '<%= appConfig.srcPath %>/less/**/*.less',
          ],
          options: {
              html: {
                  fileTypes: [".html", ".php"],
                  braceStyle: "collapse",
                  indentChar: " ",
                  indentScripts: "keep",
                  indentSize: 4,
                  maxPreserveNewlines: 10,
                  preserveNewlines: true,
                  unformatted: ["a", "sub", "sup", "b", "i", "u"],
                  wrapLineLength: 0
              },
              css: {
                  fileTypes: [".less", ".css"],
                  indentChar: " ",
                  indentSize: 4
              },
              js: {
                  braceStyle: "collapse",
                  breakChainedMethods: false,
                  e4x: false,
                  evalCode: false,
                  indentChar: " ",
                  indentLevel: 0,
                  indentSize: 4,
                  indentWithTabs: false,
                  jslintHappy: false,
                  keepArrayIndentation: false,
                  keepFunctionIndentation: false,
                  maxPreserveNewlines: 10,
                  preserveNewlines: true,
                  spaceBeforeConditional: true,
                  spaceInParen: false,
                  unescapeStrings: false,
                  wrapLineLength: 0,
                  endWithNewline: true
              }
          }
      },

      modernizr: {
          dist: {
              "parseFiles": true,
              "dest": "<%= appConfig.distPath %>/js/modernizr.min.js",
              "uglify": true
          }
      },

      copy: {
        img: {
          expand: true,
          cwd: '<%= appConfig.srcPath %>/img',
          src: ['**'],
          dest: '<%= appConfig.distPath %>/img/'
        },
        fonts: {
          expand: true,
          cwd: '<%= appConfig.srcPath %>/fonts',
          src: ['**'],
          dest: '<%= appConfig.distPath %>/fonts/'
        },
        fontsBootstrap: {
          expand: true,
          cwd: '<%= appConfig.bowerComponentsPath %>/bootstrap/dist/fonts',
          src: ['**'],
          dest: '<%= appConfig.distPath %>/fonts/bootstrap'
        },
        jsBootstrap: {
          expand: true,
          cwd: '<%= appConfig.bowerComponentsPath %>/bootstrap/dist/js',
          src: ['bootstrap.min.js'],
          dest: '<%= appConfig.distPath %>/js'
        },
        fontsFontAwesome: {
          expand: true,
          cwd: '<%= appConfig.bowerComponentsPath %>/font-awesome/fonts',
          src: ['**'],
          dest: '<%= appConfig.distPath %>/fonts/font-awesome'
        },
        jsjQuery: {
          expand: true,
          cwd: '<%= appConfig.bowerComponentsPath %>/jquery/dist',
          src: ['jquery.min.js'],
          dest: '<%= appConfig.distPath %>/js'
        },
        jsRespond: {
          expand: true,
          cwd: '<%= appConfig.bowerComponentsPath %>/respond/dest',
          src: ['respond.min.js'],
          dest: '<%= appConfig.distPath %>/js'
        },
      },

      watch: {
        options: {
          livereload: true,
        },
        templates: {
          files: ['<%= appConfig.rootPath %>/**/*.php', '<%= appConfig.rootPath %>/**/*.html'],
          tasks: ['jsbeautifier']
        },
        css: {
          files: ['<%= appConfig.srcPath %>/less/**/*.less'],
          tasks: ['clean:css', 'jsbeautifier', 'less']
        },
        js: {
          files: ['<%= appConfig.srcPath %>/js/**/*.js'],
          tasks: ['clean:js', 'jsbeautifier', 'concat', 'copy:jsBootstrap', 'copy:jsjQuery', 'copy:jsRespond', 'modernizr']
        },
        img: {
          files: ['<%= appConfig.srcPath %>/img/**/*.*'],
          tasks: ['clean:img', 'copy:img']
        },
        fonts: {
          files: ['<%= appConfig.srcPath %>/fonts/**/*.*'],
          tasks: ['clean:fonts', 'copy:fonts', 'copy:fontsBootstrap', 'copy:fontsFontAwesome']
        }
      }

    });


    // definice tasku
    grunt.registerTask('default', ['clean', 'jsbeautifier', 'less', 'concat', 'copy', 'modernizr']);
    grunt.registerTask('watcher', ['default', 'watch']);

};
